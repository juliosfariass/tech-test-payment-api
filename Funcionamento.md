# API DE VENDAS
A API foi criada utilizando .NET e SQL Server. Tem a função de registrar Vendedores e Vendas no banco de dados.  
<br>

<div align="center">
<img src="/uploads/095150c99c8fae80d584a47a795117a0/1.png" width="700px" />
</div>  

<div align="center">
  <em>Visão geral da API no Swagger.</em>
</div>  
<br>

# VENDEDORES

Para registrar uma Venda é necessário que um Vendedor já tenha sido registrado.

## Registrar Vendedor

Para registrar um Vendedor devem ser passados o nome, CPF, email e telefone. O ID do Vendedor é gerado automaticamente pelo banco de dados.

<div align="center">
<img src="/uploads/915da15be55788ab0f105319a98f5474/2.png" width="700px" />
</div>  

<div align="center">
  <em>Criação de um Vendedor.</em>
</div>  
<br>

<div align="center">
<img src="/uploads/f25d6744888e76a4e552b6ec281f66ec/3.png" width="700px" />
</div>  

<div align="center">
  <em>Resposta recebida ao criar um Vendedor.</em>
</div>  
<br>

## Obter Vendedores

Vendedores podem ser obtidos através de seu ID ou através de uma listagem geral.

<div align="center">
<img src="/uploads/71fc7c39d323542281bc916c982fe713/4.png" width="700px" />
</div>  

<div align="center">
  <em>Vendedor obtido por ID.</em>
</div>  
<br>

<div align="center">
<img src="/uploads/bf9c709f8ce04b72ff457cc6533b7e1b/5.png" width="700px" />
</div>  

<div align="center">
  <em>Listagem geral de Vendedores no banco de dados.</em>
</div>  
<br>

## Modificar Vendedor

Para modificar as informações de um Vendedor é necessário informar o seu ID.

<div align="center">
<img src="/uploads/0043041cad9cbe08f39054f015e7e3ff/6.png" width="700px" />
</div>  

<div align="center">
  <em>Modificando Vendedor de ID com valor 1.</em>
</div>  
<br>

<div align="center">
<img src="/uploads/9a621b74b2212c792a35806487b402ad/7.png" width="700px" />
</div>  

<div align="center">
  <em>Resposta de modificação.</em>
</div>  
<br>

# VENDAS

## Registrar Venda

Para registrar uma Venda é necessário passar o ID do Vendedor e os itens do pedido.

<div align="center">
<img src="/uploads/37f5965fe4520952527eeea9abd7b12f/8.png" width="700px" />
</div>  

<div align="center">
  <em>Registrar Venda.</em>
</div>  
<br>

A Venda é registrada com a data atual, com o status "Aguardando pagamento" e mostra as informações do Vendedor obtido através do ID.

<div align="center">
<img src="/uploads/328f37572da09bc709a783a35a180702/11.png" width="700px" />
</div>  

<div align="center">
  <em>Resposta de registro da Venda.</em>
</div>  
<br>

Caso o ID do Vendedor não seja encontrado ou nenhum item seja informado, são mostrados avisos e a Venda não é registrada.

<div align="center">
<img src="/uploads/fc674fc75d99f28db9feaff516554177/9.png" width="700px" />
</div>  

<div align="center">
  <em>Vendedor não encontrado.</em>
</div>  
<br>

<div align="center">
<img src="/uploads/0bb79f2078edb6d53de52832c24e77b8/10.png" width="700px" />
</div>  

<div align="center">
  <em>A Venda deve possuir 1 item no mínimo.</em>
</div>  
<br>

## Buscar Venda

Uma Venda pode ser obtida através do seu ID, trazendo as informações do pedido e do Vendedor.

<div align="center">
<img src="/uploads/9a9b918cbd8653e9786135ce5b4853bc/12.png" width="700px" />
</div>  

<div align="center">
  <em>Resposta da busca de Venda por ID.</em>
</div>  
<br>

Caso a Venda não seja encontrada, é mostrado um aviso.

<div align="center">
<img src="/uploads/8abb553852cc73a58c174e31f7e43a92/13.png" width="700px" />
</div>  

<div align="center">
  <em>Venda não encontrada no banco de dados.</em>
</div>  
<br>

## Atualizar Venda

Usado para atualizar o status do pedido ao passar seu ID e o novo status. Os status são:

- 0 "Aguardando Pagamento" : Só pode ser atualizado para "Pagamento Aprovado" ou "Cancelada".
- 1 "Pagamento Aprovado" : Só pode ser atualizado para "Enviado Para Transportadora" ou "Cancelada".
- 2 "Enviado Para Transportadora" : Só pode ser atualizado para "Entregue".
- 3 "Entregue" : Não pode ser atualizado.
- 4 "Cancelada" : Não pode ser atualizado.

<div align="center">
<img src="/uploads/42c9514585ab71524412a483381b4ef3/14.png" width="700px" />
</div>  

<div align="center">
  <em>Exemplo de atualização permitida.</em>
</div>  
<br>

<div align="center">
<img src="/uploads/0720376f9c603c683680bb8f8e9f2886/15.png" width="700px" />
</div>  

<div align="center">
  <em>Exemplo de atualização não permitida.</em>
</div>  
<br>
